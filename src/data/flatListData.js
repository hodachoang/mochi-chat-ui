var flatListData = [
    {
        "key": "1",
        "name": "Jennifer Aniston",
        "description": "How are you??",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': true,
        'status': false
    },
    {
        "key": "2",
        "name": "Angelina Jolie",
        "description": "I'm fine. And you?",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': true

    },
    {
        "key": "3",
        "name": "Megan Fox",
        "description": "I'm very well, thanks.",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': true,
        'status': false

    },
    {
        "key": "4",
        "name": "Mila Kunis",
        "description": "How old are your?",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': true

    },
    {
        "key": "5",
        "name": "Anne Hathaway",
        "description": "I'm 22 years old.",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': true,
        'status': false

    },
    {
        "key": "6",
        "name": "Rosamund Pike",
        "description": "Where are you from?",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': true

    },
    {
        "key": "7",
        "name": "Natalie Portman",
        "description": "I'm from Quang Tri",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': false

    },
    {
        "key": "8",
        "name": "Natalie Portman",
        "description": "Do you speak English?",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': true,
        'status': true

    },
    {
        "key": "9",
        "name": "Natalie Portman",
        "description": "Yes. I can speak English",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': false

    },
    {
        "key": "10",
        "name": "Natalie Portman",
        "description": "What time is it?",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': true

    },
    {
        "key": "11",
        "name": "Natalie Portman",
        "description": "It's 7 PM ",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': false,
        'status': false

    },
    {
        "key": "12",
        "name": "Natalie Portman",
        "description": "It's too late",
        'imageUrl' : "https://www.dkn.tv/wp-content/uploads/2017/04/Sinh-con-gai-lai-hon.jpg",
        'meg': true,
        'status': true

    },
];
module.exports = flatListData;// public mảng flatlistdata để các file có thể sử dụng