
import React, {Component} from 'react';
import {Platform, StyleSheet, Image,Text, View} from 'react-native';
import Logo from '../assets/logo.png';
import styles from '../constans/styles';

export default class Splash extends Component {
  loading = async ()=>{
    this.props.navigation.navigate('App');
    await AsyncStorage.setItem('userToken', 'abc');
  }
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={Logo} 
        />
        <Text  style={styles.mochi}>Mochi</Text>
       
      
      </View>
    );
  }
}

