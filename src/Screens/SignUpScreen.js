import React from 'react';
import { View, Image, Text,TextInput, TouchableOpacity, Alert,ScrollView} from 'react-native';
import { SocialIcon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import _ from 'lodash'
import Logo from "../assets/logo.png"
import styles from '../constans/styles'
export default class App extends React.Component {
  static navigationOptions ={
    header: null,
    headerLeft: null
  };
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      name: '',
      index: 0,
      email:'',
      isValidateEmail: null,
      isValidatePassword: true,
      
    }
  }

  submitForm = ()=> { 
    if(this.state.password.length <6 ){
      Alert.alert('Error', 'lenght password >6 ')
    }else if(this.state.confirmPassword !== this.state.password){
      Alert.alert('Error', 'Wrong confirm Password')
    }else{
     
      this.props.navigation.navigate('ChatScreen')

    }
  }
  _login = () => { 
    
    this.props.navigation.navigate('LogIn');
  };

  onFocusChange = (index) => {
    this.setState({ index });
  }
  handleChange= key=>val =>{
    this.setState({[key]:val}, () => this.validate())
  }

  onShowIcon = () => {
    const { password, confirmPassword } = this.state
    
    if (!_.isEmpty(password) ) {

      if (password.length >= 6){
      // this.state({isValidateForm:true});
       return    <Icon name="check" style={styles.checkIcon} />
      }
      else {
        return <Icon name="close" style={styles.closeIcon} />
      }
    }

    else {
      return;
    }
  }
  validate = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(reg.test(this.state.email) === false)
    {
    console.log("Email is Not Correct");
    this.setState({isValidateEmail:false})
    return false;
      }
    else {
      this.setState({isValidateEmail: true})
      console.log("Email is Correct");
    }
    }

  onShowIcon2 = () => {
    const { password, confirmPassword } = this.state
    if (!_.isEmpty(confirmPassword) ) {

      if ((confirmPassword.length >= 6 && confirmPassword === password )){
  
       return    <Icon name="check" style={styles.checkIcon} />
      }
      else {
        return <Icon name="close" style={styles.closeIcon} />
      }
    } else {
      return;
    }
  }
  onShowIcon3 =() => {
    const { isValidateEmail,email } = this.state
    if (!_.isEmpty(email) ) {
      if (isValidateEmail){
       return    <Icon name="check" style={styles.checkIcon} />
      }
      else {
        return <Icon name="close" style={styles.closeIcon} />
      }
    } else {
      return;
    }
  }
  
  render() {
    return (
      <ScrollView>
      <View style={styles.containerForm}>
     
        <View style={styles.mainWrapper}>
          <Image source={Logo} style={styles.logoForm} />
        </View>
        <Text style={styles.textForm}>Enter your email ID</Text>
        <View style={styles.Form}>
          <View style={styles.inputForm} />
          <TextInput
            keyboardDismissMode='on-drag'
            onChangeText={this.handleChange("email")}
            onFocus={() => this.onFocusChange(1)}
            style={[
              styles.inputText,
              { backgroundColor: this.state.index === 1 ? "#FAFFF2" : "#FFF" }
            ]}
            placeholder="your.name@email.com"
            leftIcon={{ type: "font-awesome", name: "check" }}
          />
          {this.onShowIcon3()}

          <Text style={styles.textForm}>Type your password</Text>
          <View>
            <TextInput
              keyboardDismissMode='on-drag'
              onFocus={() => this.onFocusChange(2)}
              style={[
                styles.inputText,
                { backgroundColor: this.state.index === 2 ? "#FAFFF2" : "#FFF" }
              ]}
              value={this.state.password}
              onChangeText={this.handleChange("password")}
              leftIcon={{ type: "font-awesome", name: "check" }}
              secureTextEntry={true}
              password={true}
            />
            {this.onShowIcon()}
          </View>
          <Text style={styles.textForm}>Retype your password</Text>
          <View>
            <TextInput
              keyboardDismissMode='on-drag'
              onFocus={() => this.onFocusChange(3)}
              style={[
                styles.inputText,
                { backgroundColor: this.state.index === 3 ? "#FAFFF2" : "#FFF" }
              ]}
              leftIcon={{ type: "font-awesome", name: "check" }}
              secureTextEntry={true}
              value={this.state.confirmPassword}
              onChangeText={this.handleChange("confirmPassword")}
            />
            {this.onShowIcon2()}
          </View>
        </View>
        <View style={styles.footer}>
        <Text style={styles.description}>Link your social media profiles </Text>
        <View style={styles.Direction}>
            <SocialIcon style={styles.socialMediaLogin} type="facebook" />
            <SocialIcon style={styles.socialMediaLogin} type="twitter" />
            <SocialIcon style={styles.socialMediaLogin} type="google-plus-official" />
          </View>
          <TouchableOpacity style={styles.buttonSignUp} onPress={this.submitForm}>
              <Text style={styles.loginText}>SIGNUP</Text>
            </TouchableOpacity>
            <View style={styles.Direction}>
            <Text style={styles.linkLogin} onPress={this._login}>Login </Text>
            <Text style={styles.description}>here if you already have an account ! </Text>

          </View>
      
        </View>
      </View>
      </ScrollView>
    );
  }
}


