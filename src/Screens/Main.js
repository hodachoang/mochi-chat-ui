
import React from 'react';
import { TabNavigator, createAppContainer, createStackNavigator, createBottomTabNavigator,createTabNavigator } from 'react-navigation';
import { Text, View,ScrollView,Button, } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import IconLeft from 'react-native-vector-icons/AntDesign'
import chats from './SearchChatList';
import groups from './GroupsChatList';
import contacts from './ContactList';
import settings from './Settings';
import styles from '../constans/styles'


const MainNavigator = createBottomTabNavigator({
  chats: {
    screen: chats,
  },
  groups: { 
    screen: groups
  },
  contacts: {
    screen: contacts
  },
  settings: {
    screen: settings
  }
}, {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      style: {
        backgroundColor: 'white',
        height: 60
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',

    }

  });
const MainTest = createAppContainer(MainNavigator);
export default class Main extends React.Component {
  static navigationOptions = {
    header: null,
    headerLeft: null,
    headerRight:null,
    tabBarIcon: ({ tintColor }) => {  
      const iconColor = tintColor === 'black' ? 'white' : '#A8E490'
      const backgroundIconColor = tintColor === 'black' ? '#A8E490' : 'white'
      if(tintColor === 'black'){
      }
      return <View style={{
        backgroundColor: backgroundIconColor,
        width: 40,
        height: 40,
        borderRadius: 20,
        padding: 10,
      }}>
        <Icon name="comments-o" size={20} style={{ color: iconColor }} />
      </View>
    },
  }
  render() {
    return (<MainTest />
    );
  }
}

