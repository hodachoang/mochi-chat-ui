import Icon from 'react-native-vector-icons/AntDesign';
import { Container, Text, Content}  from 'native-base';
import React, { Component } from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import styles from '../constans/styles';

export default class settings extends Component{
    static navigationOptions = {
        
        tabBarIcon: ({tintColor}) =>{
            const iconColor = tintColor==='black'? 'white' : '#A8E490'
            const backgroundIconColor =  tintColor==='black'? '#A8E490' : 'white'
            return <View
            style={{backgroundColor: backgroundIconColor, 
            width: 40,
            height:40,
            borderRadius: 20,
            padding: 10,
            }}
            >
                <Icon name="setting" size={20} style={{color: iconColor}} />
            </View>
        }
    }
    render(){
        return (
            <View style={styles.setting}>
           
                <Icon name="setting" size={200} style={{color:'#A8E490'}} />
              
            </View>
        );
    }
}
