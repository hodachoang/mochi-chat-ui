import React from 'react'
import { Text,View,TextInput,Image } from 'react-native'
import { GiftedChat,Bubble,MessageText,Time, InputToolbar } from 'react-native-gifted-chat'
import IconInfo from 'react-native-vector-icons/AntDesign'
import IconLeft from 'react-native-vector-icons/AntDesign'
import Icon from 'react-native-vector-icons/AntDesign'
import styles from '../constans/styles'
import voice from "../assets/voice-copy-symbol-instanc.png"
import emoji from "../assets/emoji.png"
import Send from "./Chat/Send"

export default class GroupChatScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return{
   
    headerRight: <IconInfo  style={styles.iconHeader} name="infocirlceo" color="#a8e490" size={30}   onPress={navigation.getParam('userInfo')}/>,
    headerTitle: <Text style={styles.headerTitleStyle}>Larry Page</Text> ,
    headerLeft: <IconLeft  style={styles.iconHeader} name="left" color="#a8e490" size={30}  onPress={navigation.getParam('back')} /> 
  }
}
componentDidMount() {
  this.props.navigation.setParams({ back: this._Back });
  this.props.navigation.setParams({ userInfo: this._UserInfo });
}
_Back=()=>{
  this.props.navigation.goBack()

}
_UserInfo=()=>{
  this.props.navigation.navigate('GroupUserInfo')
  
}
  constructor(props) {
    super(props);
    state = {
      messages: [],
      typingText: null,
    }
  }

 
  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
        {
          _id: 2,
          text: 'Hello Everyone',
          createdAt: new Date(),
          user: {
            _id: 10,
            name: 'React Native',
            avatar: 'https://placeimg.com/640/480/people/sepia',
          },
        },
      ],
    })
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }
  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: 
            styles.bubblesRight
        }}
      />
    );
  }
  renderMessageText = (props) => (
   <MessageText
      {...props} 
      customTextStyle={{color: 'black'}}
      textStyle={{
        right: {
          color: 'black'
        },
      }}
   />
  )
  renderSend(props) {
    return (
      <View style={{  flexDirection: 'row'}}>
        <Send 
            {...props}
        >
            <View style={styles.upcircleIcon}><Icon name="upcircle" size={25}  /></View>
           
        </Send>
         <View style={styles.composer} >
          <Icon style={styles.cameraIcon} name="camera" size={25} />
          <Image source={emoji}  right={55} top={5} />
          <Image source={voice} size={25}/>
         </View>
      
        </View>
    );
}
 

  renderComposer(props) {
   
    return(
      <View style={styles.composer}  > 
      <View style={styles.keyboarBar} >
            <Image source={emoji}  right={10} marginTop={10}/>
    </View>
          <View style={styles.cameraKeyboar}><Icon name="camera" size={25} /></View>
          <View>  
           <Image source={voice} size={25}/>
        </View>
    </View>
  
  )
}


  renderTime = (props) => (
    <Time
       {...props} 
       timeTextStyle={{
         right:{
           color:'gray'
         }
       }}
    />
   )
  render() {
    return (
      <GiftedChat
        renderBubble={this.renderBubble}
        renderTime={this.renderTime}
        renderMessageText={this.renderMessageText}
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
       // renderComposer={this.renderComposer}
       renderSend = {this.renderSend}
       
        user={{
          _id: 1,
         
        }}
      /> 
    )
  }
}