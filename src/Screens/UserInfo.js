import React, { Component } from 'react';
import { StyleSheet, View,  Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/EvilIcons'
import IconDelete from 'react-native-vector-icons/AntDesign'
import IconHistory from 'react-native-vector-icons/MaterialCommunityIcons'
import IconBlock from 'react-native-vector-icons/Feather'
import IconLeft from 'react-native-vector-icons/AntDesign'
import styles from '../constans/styles'
const listItem = [
  {
    title: 'View Profile',
    icon: 'user',
    screenName: 'about',
    typeIcon: 'icon'
  },
  {
    title: 'Delete Conversation',
    icon: 'trash',
    screenName: 'about',
    typeIcon: 'icon'
  },
  {
    title: 'Report Conversation',
    icon: 'message1',
    screenName: 'abou323t',
    typeIcon: 'delete'

  },
  {
    title: 'View Chat History',
    icon: 'history',
    screenName: 'about2323',
    typeIcon: 'history'

  },
  {
    title: 'Block User',
    icon: 'user-minus',
    screenName: 'aboutss',
    typeIcon:'block'
  },
  // more items
]

export default class Settings extends Component {
  static navigationOptions = ({ navigation }) => {
    return{
    headerTitle: <Text style={styles.headerTitleStyle}>Larry Page</Text> ,
    headerLeft: <IconLeft name="left" color="#a8e490" size={30}  onPress={navigation.getParam('back')}  /> ,

  
    }};
  componentDidMount() {
    this.props.navigation.setParams({ back: this._Back });
  }
  _Back=()=>{
    this.props.navigation.goBack()
  
  }
  onChangeScreen = (screenName) => {
    alert(screenName)
  }

  _renderIcon = (typeIcon, icon) => {
    switch (typeIcon) {
      case 'icon':
        return  <Icon
                  size={30}
                  color='#ffffff'
                  name={icon}
                />
      case 'delete':
        return  <IconDelete
                 style={styles.IconDelete}
                  size={20}
                  color='#ffffff'
                  name={icon}
                />
       case 'history':         
       return <IconHistory
       style={styles.IconHistory}
       size={25}
       color='#ffffff'
       name={icon}

       />
       case 'block':
       return <IconBlock
       style={styles.IconBlock}
       size={20}
       color='#ffffff'
       name={icon}
       />
      default: return null
    }
  }

  _renderListView = () => (
    listItem.map((item, index) => {
      return (





        <TouchableOpacity
          onPress={() => this.onChangeScreen(item.screenName)}
          style={styles.TouchableOpacity}
          key={index}
        >
          <View style={styles.IconBorder}
          >
            {
              this._renderIcon(item.typeIcon,item.icon)

            }

          </View>

          <View
            style={styles.textStyle}
             
          
          >
            <Text style={styles.titleStyle}

              

            >{item.title}</Text>


            <Icon
              name='chevron-right'
              size={45}
              color='#a8e490'
            />

          </View>


        </TouchableOpacity>



      )
    })
  )
  render() {
    return (
      <View style={styles.listStyle}>
        {this._renderListView()}
      </View>

    );
  }
}

