  import React from 'react';
import { StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ScrollView} from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import { SocialIcon } from 'react-native-elements'
import Logo from "../assets/logo.png"
import styles from '../constans/styles'

export default class App extends React.Component {
  static navigationOptions ={
    header: null,
    headerLeft: null
  };
  state={
    isOn: false,
    color:false,
    handleTextinput:1,
    index: 0
  } 
  onFocusChange = (index) => {
    this.setState({ index });
  }

  SwitchGo = (isOn) => {

  
    this.setState({ isOn })
  }
  _submit = () => {
    
    this.props.navigation.navigate('Main');
  };
  _signUp = () =>{
    this.props.navigation.navigate('SignUp');

  }
render() {
    
    return (
      <ScrollView>
      <View style={styles.containerForm}>
        <View style={styles.mainWrapper}>
          <Image source={Logo} style={styles.logoForm} />
        </View>
        <View style={styles.Form}>
          <Text style={styles.textForm}>Username</Text>
          <TextInput
            onFocus={() => this.onFocusChange(1)}
            style={[
              styles.inputText,
              { backgroundColor: this.state.index === 1 ? "#FAFFF2" : "#FFF" }
            ]}
            placeholder="your.name@email.com"
          />
          <Text style={styles.textForm}>Password</Text>

          <TextInput
            onFocus={() => this.onFocusChange(2)}
            style={[
              styles.inputText,
              { backgroundColor: this.state.index === 2 ? "#FAFFF2" : "#FFF" }
            ]}
            secureTextEntry={true}
            password={true}
          />
          <ToggleSwitch
            isOn={this.state.isOn}
            onColor="#6DCAF3"
            offColor="#6DCAF3"
            label="Rememberme"
            labelStyle={{ color: "#343434", marginRight: "42%" }}
            size="small"
            onToggle={isOn => this.SwitchGo(isOn)}
          />
        </View>
      
        <View style={styles.footer}>
        <TouchableOpacity style={styles.button}
        onPress={this._submit}>
            <Text style={styles.loginText}>LOGIN</Text>
          </TouchableOpacity>
          <Text style={styles.description}>You can also login with … </Text>
          <View style={styles.Direction}>
            <SocialIcon style={styles.socialMediaLogin} type="facebook" />
            <SocialIcon style={styles.socialMediaLogin} type="twitter" />
            <SocialIcon style={styles.socialMediaLogin} type="google-plus-official" />
          </View>
          <View style={styles.Direction}>
            <Text style={styles.description}>Not registered yet?</Text>
            <Text style={styles.linkLogin}  onPress={this._signUp}>Sign up!</Text>
          </View>
        </View>
      </View>
      </ScrollView>
    );

  }
}

