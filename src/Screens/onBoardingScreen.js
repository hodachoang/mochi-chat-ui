import React from 'react';
import {Platform,SafeAreaView ,Image,StyleSheet, Text,TouchableOpacity, View, Dimensions} from 'react-native';
import Carousel from 'react-native-snap-carousel'
import styles from '../constans/styles'
import Splash from './Splash'


const { height, width } = Dimensions.get('window');


export default class onBoardingScreen extends React.Component {
  
  static navigationOptions ={
    header: null,
    headerLeft: null
  };
  constructor(props) {
    super(props);
    this.state={
      loading:false,
      activeSlide:false,
      activeIndex:0,
      paginations:[0,1,2],
    
      carouselItem:[
        {
          title:"connect with friends!",
          // image:"01",
          image:require(`../assets/01.png`),
          content:"Sed ut perspiciatis unde omnis iste rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae."
        },
        { 
          title:"get in touch instantly",
          // image:"02",
          image:require(`../assets/02.png`),
          content:"Eaque ipsa quae abed ut perspiciatis unde omnis iste rem aperiam, eaque ipsa quae ab illo inventore verita."
        },
        {
          title:"many platforms, one chat",
          // image:"03",
          image:require(`../assets/03.png`),
          content:"Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae. Isterem aperiam, eaque ab illo."
        },
      ]
        
       

    }
  }
  componentDidMount() {
    this.setState({ loading: false }, () =>  setTimeout(() => { 
    this.setState ({ loading: true})
     } , 1000))
  
  }
  _login = () => { 
    
    this.props.navigation.navigate('LogIn');
  };
  _signup =  () => {
  
    this.props.navigation.navigate('SignUp');
  };
_renderItem({item,index}){
  return(

      <View  >
         <Image style={styles.imageSlide} resizeMode="contain" source={item.image}/>
         <View style={styles.onBoardingText}>
          <Text style={styles.textTitle}>{item.title}</Text>
          <Text style={styles.textContent}>{item.content}</Text>
         </View>
      </View>

  )  
}


  render() {
    return (

      <SafeAreaView style={styles.container}>
       { !this.state.loading ? <Splash /> :
       <View>
         <Carousel 
          style={styles.viewItem}
          ref ={ref => this.carousel = ref}
          data ={this.state.carouselItem}
          sliderWidth = {width}
          itemWidth = {width}
          itemHeight={height}
          renderItem = {this._renderItem}
          onSnapToItem = {
            index=>{ this.setState({activeIndex:index})  }
          }
         />
         <View style={styles.pagination}>
           {this.state.paginations.map((pagination,k)=>{
             return (
              <TouchableOpacity
               key={k}
              style={[styles.onePagination ,{backgroundColor: this.state.activeIndex === pagination ? '#a8e490' : '#e5e5e561'}]}
                onPress={()=> this.carousel.snapToItem(pagination)}
              />
             )})} 
         </View>
        
         <View  style={styles.buttonOnBoarding}>
            <TouchableOpacity style={styles.buttonLogin}
             onPress={this._signup} >
              <Text style={styles.buttonText}>SIGN UP </Text>
              
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonLogin}
             onPress={this._login} >
              <Text style={styles.buttonText}> LOGIN </Text>
            </TouchableOpacity>
        </View>
        </View>
      }
      </SafeAreaView>
    );
  }
}

