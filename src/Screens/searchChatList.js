
import React, { Component } from 'react';
import IconF from 'react-native-vector-icons/FontAwesome';
import { Alert, AppRegistry, FlatList, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import flatListData from './../data/flatListData';
import Swipeout from 'react-native-swipeout';
import { Avatar, Badge } from 'react-native-elements';
import styles from '../constans/styles';

export default class searchChatList extends React.Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => {
      return <View 
      style={tintColor === 'black' ? styles.backgroundIconTabBarChoose : 
      styles.backgroundIconTabBar}>
        <IconF 
        name="comments-o" 
        size={20} 
        style={tintColor === 'black' ? styles.iconTabBarChoose : styles.iconTabBar} />
      </View>
    },
    title: 'chats',
    header: null,
  }
  constructor(props) {
    super(props);
    this.state = ({
      deleteRowKey: null,
      activeRowkey: null
    });
  }
  refreshFlatList = (deleteKey) => {
    this.setState((prevState) => {
      return {
        deleteRowKey: deleteKey
      };
    });
  }
  
  openChatWindow = () => {
    this.props.navigation.navigate('ChatWindow');
  };

  isMessage = (text) => {
    if (text)
      return <Badge
        status="warning"
        containerStyle={styles.containerStyleBadgeItemView}
        value="1"
        textStyle={styles.textStyleBadgeItemView}
      />
  };

  render() {
    const swipeSettings = {
      backgroundColor: '#ffff',
      autoClose: true,
      onClose: (secId, rowId, derection) => {
      },
      onOpen: (secId, rowId, direction) => {
      },
      right: [
        {
          onPress: () => { },
          component: (
            <View style={styles.componentView}>
              <View style={styles.chatItemAction1}>
                <IconF name="comment-o" size={35} color="#fff" />
                <IconF name="check" size={15} color="#fff" style={styles.inCommentIcon} />
              </View>
            </View>
          ),
          type: 'secondary',
          backgroundColor: '#ffff'
        },
        {
          onPress: () => { },
          backgroundColor: '#ffff',
          component: (
            <View style={styles.componentView}>
              <View style={styles.chatItemAction2}>
                <IconF name="comment-o" size={35} color="#fff" />
                <IconF name="times" size={15} color="#fff" style={styles.inCommentIcon}/>
              </View>
            </View>
          ),
        }
      ],
      rowId: this.props.index,
      sectionId: 1
    };

    return (
      <View style={styles.backgroundMain}>

        <View style={styles.headerMain}>
          <IconF name="angle-left" size={35} color="#A8E490" style={styles.angleLeftHeaderMain}  onPress={() => this.props.navigation.goBack()} />
          <Text style={styles.titleHeaderMain}>
            Your group chats
          </Text>
          <IconF name="bell-o" size={23} color="#A8E490" style={styles.bellRightHeaderMain}/>
          <Badge
            status="warning"
            containerStyle={styles.badgeContainerStyleHeader}
            value="2"
            textStyle={{ fontSize: 15 }}
          />
          <IconF name="edit" size={23} color="#A8E490" style={styles.editRightHeaderMain} />
        </View>

        <View style={styles.mainContent}>
          <View style={styles.caterogrySearch}>
            <IconF name="search" size={20} color="black" style={styles.iconSearch} />
            <TextInput
              placeholder="Search"
              placeholderTextColor="black"
              style={styles.textInputSearch}
            />
          </View>
          <View style={{ flex: 55 }}>
            <View style={styles.caterogryView}>
              <Text style={styles.categoryItem}>Recent chats</Text>
            </View>
            <FlatList
              data={flatListData}
              renderItem={({ item, index }) => {
                return (
                  <View>
                    <View
                      style={styles.borderTopView}
                    />
                    <Swipeout {...swipeSettings} item={item} index={index}>
                      <View style={styles.itemView}  >
                        <Avatar
                        
                          onPress={this.openChatWindow}
                          source={{ uri: item.imageUrl }}
                          rounded
                          size={40}
                          containerStyle={{ marginRight: 15 }}

                        />
                        <View style={styles.flatListColumn}>
                          <TouchableOpacity onPress={this.openChatWindow} >
                            <Text style={styles.textItemName}>{item.name}</Text>
                          </TouchableOpacity>
                          <View style={styles.viewflatListColumn}>
                            <IconF name="circle" size={7} color={(item.status === true) ? '#7ed321' : '#d0021b'} style={styles.iconStatus} />
                            <Text style={item.meg === true ? styles.textItemChatNote : styles.textItemChat}   >
                              {item.description}
                            </Text>
                          </View>

                        </View>
                        {this.isMessage(item.meg)}
                      </View>
                    </Swipeout>
                  </View>
                )
              }}
            >
            </FlatList>
          </View>
          <View style={{ flex: 35 }}>
            <View style={styles.caterogryView}>
              <Text style={styles.categoryItem}>Advailiable Friends</Text>
            </View>
            <FlatList
              data={flatListData}
              renderItem={({ item, index }) => {
                return (
                  <View>
                    <View
                      style={styles.borderTopView}
                    />
                    <View style={styles.itemView}>
                      <Avatar
                        onPress={this.openChatWindow}
                        source={{ uri: item.imageUrl }}
                        rounded
                        size={40}
                        containerStyle={{ marginRight: 15 }}
                      />
                      <View style={styles.flatListColumn}>
                        <TouchableOpacity onPress={this.openChatWindow}>
                          <Text style={styles.textItemName}>{item.name}</Text>
                        </TouchableOpacity>
                        <View style={styles.viewflatListColumn}>
                          <IconF name="circle" size={7} color={(item.status === true) ? '#7ed321' : '#d0021b'} style={styles.iconStatus} />
                          <Text style={styles.textItemChat}>
                            {(item.status) ? 'online' : 'offline'}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                )
              }}
            >
            </FlatList>
          </View>
        </View>
      </View>
    );
  }
}
