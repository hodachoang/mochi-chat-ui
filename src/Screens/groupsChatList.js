
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Text, View, ScrollView, Button } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';
import IconAnt from 'react-native-vector-icons/AntDesign'
import styles from '../constans/styles'
const listView = [
  {
    id: 1,
    usernumber: 3,
    name: 'Chris, Larry, Mark',
    avatar: 'user',
    message: 'I tested the UI. It works!',
    screenName: 'about1',
    activebadge: 1

  },
  {
    id: 2,
    usernumber: 2,
    name: 'Scarlett, Robert',
    avatar: '../assets/avatar1.jpg',
    message: 'call me by 4? I’ll be done by then.',
    screenName: 'about2',
    activebadge: 2
  },
  {
    id: 3,
    usernumber: 3,
    name: 'Steven, Jeffrey, Devid',
    avatar: 'user',
    message: 'the new movie is great.',
    screenName: 'about3',
    activebadge: 2

  },

  // more items
]
export default class groupsChatList extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => {
      return <View 
      style={tintColor === 'black' ? styles.backgroundIconTabBarChoose : styles.backgroundIconTabBar}
      >
        <Icon 
        name="group" 
        size={20} 
        style={tintColor === 'black' ? styles.iconTabBarChoose : styles.iconTabBar} />
      </View> 
    },
    title: 'groups'
  }
  openGroupChatWindow = () => {
    this.props.navigation.navigate('GroupChatWindow');
  };
  openSearchChatList = () => {
    this.props.navigation.navigate('SearchChatList');
  };
  render() {
    return (
      <View style={styles.backgroundMain}>
        <View style={styles.headerMain}>
          <Icon  onPress={this.openSearchChatList} name="angle-left" size={30} color="#A8E490" style={styles.angleLeftHeaderMain} />
          <Text style={styles.titleHeaderMain}>
            Your group chats
              </Text>
          <Icon name="bell-o" size={23} color="#A8E490" style={styles.bellRightHeaderMain} />
          <IconAnt name="plus" size={30} color="#A8E490" style={styles.editRightHeaderMain} />
        </View>
        <View style={styles.mainContent}>
          <View >
            <View
              style={styles.borderTopView}
            />
            <ScrollView >
              {listView.map((user) => (
                <View key={user.id} style={styles.ScrollView}>

                  <ListItem
                    style={styles.groupListItem}
                    key={user.id}
                    roundAvatar
                    leftAvatar={user.usernumber == 2 ?
                      <View style={styles.avatarStyle}>
                        <View style={styles.avatarIcon} >
                          <Avatar size={30} rounded source={require('../assets/avatar1.png')} />
                        </View>
                        <View style={styles.avatarGroup} >
                          <Avatar size={30} rounded source={require('../assets/avatar1.png')} />
                        </View>
                      </View> :
                      <View style={styles.avatarStyle}>
                        <View style={styles.avatarIcon} >
                          <Avatar size={20} rounded source={require('../assets/avatar1.png')} />
                        </View>
                        <View style={styles.avatarGroupStyle} >
                          <Avatar size={20} rounded source={require('../assets/avatar1.png')} />
                        </View>
                        <View style={styles.avatarUserStyle} >
                          <Avatar size={20} rounded source={require('../assets/avatar1.png')} />
                        </View>
                      </View>
                    }

                    title={user.name}
                    titleStyle={styles.texttitleStyle}
                    subtitle={<View>
                      <Text style={styles.textMessage}
                      >{user.message}</Text>

                    </View>}
                   onPress={this. openGroupChatWindow}

                    badge={{
                      value: 12, textStyle: {
                        color: '#ffffff'

                      }, containerStyle: { marginTop: 5, },
                      badgeStyle: {
                        width: 23,
                        height: 22,
                        borderRadius: 40,
                        backgroundColor: '#f5a623'
                      }
                    }}


                  />
                  
                

                </View>



              ))}





            </ScrollView>

          </View>

        </View>
      </View>

    );
  }
}
