
import React, { Component } from 'react';
import IconF from 'react-native-vector-icons/FontAwesome';
import { FlatList, Text, View, Image, TextInput, TouchableOpacity,ScrollView } from 'react-native';
import flatListData from '../data/flatListData';
import Swipeout from 'react-native-swipeout';
import { Avatar, Badge } from 'react-native-elements';
import styles from '../constans/styles';
import Icon from 'react-native-vector-icons/AntDesign'

export default class contactList extends React.Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => {
      return <View
        style={tintColor === 'black' ? styles.backgroundIconTabBarChoose : styles.backgroundIconTabBar}
      >
        <IconF
          name="address-book"
          size={20}
          style={tintColor === 'black' ? styles.iconTabBarChoose : styles.iconTabBar} />
      </View>

    },
    title: 'contacts',
    header: null,
  }
  constructor(props) {
    super(props);
    this.state = ({
      deleteRowKey: null,
      activeRowkey: null
    });
  }
  refreshFlatList = (deleteKey) => {
    this.setState((prevState) => {
      return {
        deleteRowKey: deleteKey
      };
    });
  }

  openChatWindow = () => {
    this.props.navigation.navigate('ChatWindow');
  };
  openGroupsChatList = () => {
    this.props.navigation.navigate('GroupsChatList');
  };
  isMessage = (text) => {
    if (text)
      return <Badge
        status="warning"
        containerStyle={styles.containerStyleBadgeItemView}
        value="1"
        textStyle={styles.textStyleBadgeItemView}
      />
  };

  render() {
    const swipeSettings = {
      backgroundColor: '#ffff',
      autoClose: true,
      onClose: (secId, rowId, derection) => {
      },
      onOpen: (secId, rowId, direction) => {
      },
      right: [
        {
          onPress: () => { },
          component: (
            <View style={styles.componentView}>
              <View style={styles.chatItemAction1}>
                <IconF name="comment-o" size={35} color="#fff" />
              </View>
            </View>
          ),
          type: 'secondary',
          backgroundColor: '#ffff'
        },
        {
          onPress: () => { },
          backgroundColor: '#ffff',
          component: (
            <View style={styles.componentView}>
              <View style={styles.chatItemAction2}>
                <Icon name="close" size={35} color="#fff" />
              </View>
            </View>
          ),
        }
      ],
      rowId: this.props.index,
      sectionId: 1
    };

    return (
      <View style={styles.backgroundMain}>

        <View style={styles.headerMain}>
          <IconF name="angle-left" onPress={this.openGroupsChatList} size={35} color="#A8E490" style={styles.angleLeftHeaderMain} />
          <Text style={styles.titleHeaderMain}>
            Your contact
          </Text>
          <IconF name="bell-o" size={23} color="#A8E490" style={styles.bellRightHeaderMain} />
          <Badge
            status="warning"
            containerStyle={styles.badgeContainerStyleHeader}
            value="2"
            textStyle={{ fontSize: 15 }}
          />
          <IconF name="edit" size={23} color="#A8E490" style={styles.editRightHeaderMain} />
        </View>

        <View style={{ flex: 90 }}>
          <View style={{ flex: 1 }}>
            <View style={styles.caterogryView}>
              <Text style={styles.categoryItem}>Advailiable Friends</Text>
            </View>
            <ScrollView>
            <FlatList
              data={flatListData}
              renderItem={({ item, index }) => {
                if (item.status) {
                  return (
                    <View>
                      <View
                        style={styles.borderTopView}
                      />
                      <Swipeout {...swipeSettings} item={item} index={index}>
                        <View style={styles.itemView}  >
                          <Avatar
                            onPress={this.openChatWindow}
                            source={{ uri: item.imageUrl }}
                            rounded
                            size={40}
                            containerStyle={{ marginRight: 15 }}
  
                          />
                          <View style={styles.flatListColumn}>
                            <TouchableOpacity onPress={this.openChatWindow} >
                              <Text style={styles.textItemName}>{item.name}</Text>
                            </TouchableOpacity>
                            <View style={styles.viewflatListColumn}>
                            <IconF name="circle" size={7} color={(item.status === true) ? '#7ed321' : '#d0021b'} style={styles.iconStatus}/>
                              <Text style={styles.textItemChat}>
                                {(item.status) ? 'online' : 'offline'}
                              </Text>
                            </View>
  
                          </View>
                          {this.isMessage(item.meg)}
                        </View>
                      </Swipeout>
                    </View>
                  )
                }
              }}
            >
            </FlatList>
          <View style={{ flex: 1 }}>
            <View style={styles.caterogryView}>
              <Text style={styles.categoryItem}>All contact</Text>
            </View>
            <FlatList
              data={flatListData}
              renderItem={({ item, index }) => {
                return (
                  <View>
                    <View
                      style={styles.borderTopView}
                    />
                    <View style={styles.itemView}>
                      <Avatar
                        onPress={this.openChatWindow}
                        source={{ uri: item.imageUrl }}
                        rounded
                        size={40}
                        containerStyle={{ marginRight: 15 }}
                      />
                      <View style={styles.flatListColumnCenter}>
                        <TouchableOpacity onPress={this.openChatWindow}>
                          <Text style={styles.textItemName}>{item.name}</Text>
                        </TouchableOpacity>
                        
                      </View>
                    </View>
                  </View>
                )
              }}
            >
            </FlatList>
          </View>
          </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

