import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      paddingBottom:30,
      paddingTop:20
    },
    logo:  {
      width: 60,
      height: 60,
  
    },
    mochi:{
      width: 105,
      height: 45,
      color:'#a8e490',
      fontFamily: 'Quicksand',
      fontSize: 35,
      fontWeight: 'bold',
      lineHeight: 45,
      textAlign:'center',
     
    },
   
    buttonOnBoarding: {
      flexDirection: 'row',
     // marginLeft:"10%",
     justifyContent: 'center',
     textAlign:"center"
  
    },
    
    buttonLogin:{
      backgroundColor:'#a8e490',
      borderRadius:20,
      fontFamily: 'Quicksand',
      fontSize: 35,
      fontWeight: 'bold',
      width:90,
      height: 40,
      alignItems: 'center',
      padding: 10,
      marginRight:20,
      marginLeft:20
  
    },
    buttonText:{
      fontFamily: 'Quicksand',
      fontSize: 15,
      color:'white'
  
    },
    viewItem:{
    
      flexDirection:'column',
      justifyContent:'center',
      alignItems:'center',
      position:'relative',
      
    },
    imageSlide:{
      width: "100%",
      height:"55%",
      marginBottom:60,
   
    
     
    },
    onBoardingText:{
     
      justifyContent: 'center',
      textAlign:'center',
      width:"70%",
      height:"30%",
      marginLeft:"15%",
      marginTop:0,
      
    },
    textTitle:{
      justifyContent: 'center',
      textAlign:'center',
      fontFamily: 'Quicksand',
      fontSize: 20,
      fontWeight:'bold',
  
    },
    textContent:{
      justifyContent: 'center',
      textAlign:'center',
      fontFamily: 'Quicksand',
      fontSize: 15,
      marginTop:15,
     
    },
    pagination:{
      justifyContent: 'center',
      textAlign:"center",
      flexDirection: 'row',
      position:'absolute', 
      top:'55%',
      left:'25%'
    },
    onePagination:{
        width:40,
        height:10,
        margin:10,
        borderRadius:7,
        borderBottomWidth:1,
        borderColor:'#c6cbd1' ,
    },
    bubblesRight:{
      backgroundColor: '#fff0',
      marginRight:30,
      marginBottom:10,
      borderBottomWidth:1,
      borderRightWidth:1,
      borderColor:"#999",

    },
    iconHeader:{
      marginRight:10,
      marginLeft:10

    },
   
    IconDelete: {
      justifyContent: 'center',
      paddingLeft: 5 
    },
    IconBlock: {
      justifyContent: 'center', 
      paddingLeft: 6,
      
    },
    IconHistory : {
      justifyContent: 'center',
      paddingLeft: 1.5
    },
    IconBorder: {
      width: 30,
      height: 30,
      backgroundColor: '#a8e490',
      borderRadius: 50,
      paddingTop: 2,
      justifyContent: 'center'
  
  
    },
    TouchableOpacity: {
      flexDirection: 'row', 
      alignItems: 'center', 
      justifyContent: 'space-between', 
      backgroundColor: '#fafff2', 
      borderColor: '#e8e8e9' ,
      paddingLeft:13
    },
    textStyle:{
      flex: 1,
      flexDirection: 'row',
      width: 375,
      height: 50,
      backgroundColor: '#fafff2',
      paddingTop: 10,
      justifyContent: 'space-between',
      borderBottomWidth: 1,
      borderColor: '#e8e8e9'
  
    },
    titleStyle:{
      width: 200,
      height: 22,
      color: '#1d1d26',
      fontFamily: 'Quicksand',
      fontSize: 17,
      fontWeight: "400",
      paddingLeft: 14
  
    },
    listStyle:{
      flex: 1,

    },
    buttonStyle:{
      paddingTop:252,
      paddingLeft:85

    },
    buttonBorder:{
      width: 237,
      height:47,
      borderRadius: 100,
      backgroundColor: '#d0021b',
      alignItems: 'center',
      padding:14,

    },
    textColor:{
      color:'#ffffff'

    },
    headerTitleStyle:{
      fontSize:20,
      fontWeight: 'bold',
     color:"black",
      fontFamily: 'Quicksand',
      paddingLeft:'25%'
    
    },
    iconBell:{
      position: 'absolute'

    },
    iconPlus:{
      paddingLeft:55,
      paddingRight:16

    },
    ScrollView :{
      borderBottomWidth: 1,
      borderColor: '#e8e8e9',
      flex:1,
      justifyContent:'space-between'
  
    } ,
    avatarStyle:{
      position:'absolute'

    },
    avatarUserStyle:{
      paddingLeft:40, 
      position:'absolute'

    },
    avatarIcon:{
      height:15,
      paddingLeft:13

    },
    avatarGroup:{
      paddingLeft:27

    },
    avatarGroupStyle:{
      paddingLeft:27,
      paddingTop:4

    },
    texttitleStyle:{
      paddingLeft:40

    },
    textMessage:{
      color: '#4a90e2',
      fontFamily: 'Quicksand',
      fontSize: 12,
      fontWeight: '400',
      lineHeight: 11,
      paddingTop:8,
      paddingLeft:40
  
    },
    // phan hoang 
    containerForm: {
      display:'flex',
      paddingHorizontal: "15%"
    },
    mainWrapper: {
       
      justifyContent: "center",
      alignItems: "center"
    },
    logoForm: {
      marginTop:40,
      marginBottom:50,
      width: 60,
      height: 60
    },
    Form: {
  
    },
    textForm: {
      color: "#4A4A4A",
      fontSize: 14
    },
    inputText: {
      marginTop: 5,
      marginBottom: 15,
      borderRadius: 35,
      borderColor: "#A8E490",
      borderWidth: 1,
      paddingLeft: 30
    },
    button: {
      paddingLeft: 30,
      paddingRight: 30,
      borderRadius: 35,
      backgroundColor: "#A8E490",
      height: 50,
      justifyContent: "center",
      width: "100%",
      marginTop:40,
      marginBottom:40
    },
    login: {
    
    },
    loginText: {
      color: "#ffff",
      fontWeight: "bold",
      fontSize: 14,
      justifyContent: "center",
      textAlign: "center"
    },
  
    description: {
      color: "#343434"
    },
    Direction: {
      flexDirection: "row"
    },
    socialMediaLogin: {
      width: 50,
      height: 50,
      marginBottom:20,
      marginTop:30
    },
    linkLogin: {
      marginLeft: 10,
      color: "#0000FF"
    },
    inputForm: {
      position: "relative"
    },
    checkIcon: {
      color: "#A8E490",
      position: "absolute",
      right: 15,
      top: 25
    },
    closeIcon: {
      color: "red",
      position: "absolute",
      right: 15,
      top: 25
    },
    footer: {
  
      justifyContent: "space-between",
      alignItems: "center",
      flexDirection: "column",
      padding: "2%"
    },
    buttonSignUp: {
      paddingLeft: 30,
      paddingRight: 30,
      borderRadius: 35,
      backgroundColor: "#A8E490",
      height: 50,
      justifyContent: "center",
      width: "100%",
      marginBottom:20,
      marginTop:20
    },
    
/// phan cua Toan
backgroundIconTabBarChoose: {
  width: 40,
  height: 40,
  borderRadius: 20,
  padding: 10,
  backgroundColor: '#A8E490'
},
backgroundIconTabBar: {
  width: 40,
  height: 40,
  borderRadius: 20,
  padding: 10,
  backgroundColor: 'white'
},
iconTabBarChoose:{
  color: 'white'
},
iconTabBar:{
  color:'#A8E490' 
},
containerStyleBadgeItemView:{
  position: 'absolute', 
  right: 1, 
  bottom: 20
},
textStyleBadgeItemView:{
  fontSize: 15
},
containerStyleBadgeHeader:{
  position: 'absolute', 
  right: 50, 
  borderWidth: 2, 
  borderColor: 'white'
},
textStyleBadgeHeader:{
  fontSize: 15
},
itemView: {
  flex: 1,
  width: 375,
  height: 57,
  flexDirection: 'row',
  padding: 10,
  marginLeft: '3%'

},
imageItem: {
  width: 40,
  height: 40,
  borderRadius: 20,
  marginRight: 20

},
textItemName: {
  height: 14,
  color: '#1d1d26',
  fontFamily: 'Quicksand',
  fontSize: 14,
  fontWeight: "400",
  lineHeight: 14,

},
textItemChat: {
  height: 11,
  color: '#8e8e92',
  fontFamily: 'Quicksand',
  fontSize: 13,
  fontWeight: "400",
  lineHeight: 13,
  marginTop: 10,
  marginLeft: 10
},
textItemChatNote: {
  height: 11,
  color: '#3b7adb',
  fontFamily: 'Quicksand',
  fontSize: 13,
  fontWeight: "400",
  lineHeight: 13,
  marginTop: 10,
  marginLeft: 10
},
chatItemAction1: {
  width: '90%', height: '90%',
  backgroundColor: '#F5A623',
  justifyContent: "center",
  alignItems: "center",
  position: "relative"
}
,
chatItemAction2: {
  width: '90%', height: '90%',
  backgroundColor: '#D0021B',
  justifyContent: "center",
  alignItems: "center",
  position: "relative"
},
iconStatus: {
  position: 'absolute',
  bottom: 3,
  left: 0
}
,
componentView: {
  flex: 1,
  backgroundColor: '#ffff',
  justifyContent: "center",
  alignItems: "center"

},
borderTopView: {
  borderTopColor: '#E8E8E9',
  borderTopWidth: 1,
},
categoryItem: {
  fontFamily: 'Quicksand',
  fontSize: 14,
  fontWeight: 'bold',
  color: '#4a4a4a'

},
caterogryView: {
  height: 40,
  justifyContent: 'center',
  marginLeft: "5%",
  backgroundColor: '#fafff2'
}
,
caterogrySearch: {
  height: 40,
  alignItems: 'center',
  backgroundColor: '#F0F0F0',
  flexDirection: 'row'
},
iconSearch: {
  position: 'absolute', 
  left: '5%'
},
textInputSearch: {
  borderLeftColor: 'black', 
  borderLeftWidth: 1, 
  marginLeft: '15%', 
  height: '60%', 
  padding: 0, 
  paddingLeft: '3%',
},
iconHeader:{
  marginRight:20,
  marginLeft:10
}
,
  headerTitleStyle:{
    fontSize:20,
    fontWeight: 'bold',
   color:"black",
    fontFamily: 'Quicksand',
    paddingLeft:'25%'
  
  },
  toolBar:{
    marginBottom:0,
    flexDirection: 'row',
    margin:15,
    justifyContent: "center",
  
  },
  composer:{
    marginLeft:30,
    margin:10,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  upcircleIcon:{
    margin:10
  },
  
keyboarBar:{
  width: '70%',
  position:'relative'
},
keyboarInput:{
  backgroundColor:"#FFF", 
  borderColor: "#A8E490", 
  position:'absolute',
  width:'100%',
  borderWidth: 1,
  height:37, 
  borderRadius:5,
 
},
inCommentIcon:{
  position: "absolute", 
  top: '35%', 
  left: '42%'
},
headerMain: {
  flex: 10, 
  flexDirection: 'row', 
  justifyContent: 'center', 
  alignItems: 'center', 
  marginLeft: '5%', 
  marginRight: '5%'
},
backgroundMain: {
  flex: 1, 
  backgroundColor: '#ffffff' 
},
angleLeftHeaderMain: {
  position: 'absolute', 
  left: 0
},
titleHeaderMain: {
  fontSize: 20, 
  color: 'black'
},
bellRightHeaderMain: {
  position: 'absolute', 
  right: 60
},
editRightHeaderMain: {
  position: 'absolute', 
  right: 0 
},
flatListColumn: {
  flex: 1,
  flexDirection: 'column'
},
flatListColumnCenter: {
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center'
},
viewflatListColumn: {
  flex: 1, 
  flexDirection: 'row',
  position: "relative"
},
badgeContainerStyleHeader: {
  position: 'absolute', 
  right: 50, 
  borderWidth: 2, 
  borderColor: 'white' 
},
mainContent:{
  flex: 90
},
groupListItem:{
  flex: 1,
  justifyContent:'center', 
  alignItems: 'center'
},
setting:{
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  
}


  
  });
export default styles;  