import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import AuthLoadingScreen from './src/Screens/AuthLoadingScreen';
import LogInScreen from './src/Screens/LogInScreen'
import SignUpScreen from './src/Screens/SignUpScreen'
import OnBoardingScreen from './src/Screens/OnBoardingScreen'
import ChatWindow from './src/Screens/ChatWindow'
import GroupsChatList from './src/Screens/GroupsChatList'
import UserInfo from './src/Screens/UserInfo'
import GroupChatWindow  from './src/Screens/GroupChatWindow'
import SearchChatList from './src/Screens/SearchChatList'
import GroupUserInfo from './src/Screens/GroupUserInfo'
import Main from './src/Screens/Main'
// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.

const AppStack = createStackNavigator({LogIn: LogInScreen, });
const AuthStack = createStackNavigator({

  OnBoardingScreen,
  Main,
  GroupChatWindow,
  GroupUserInfo,
  SignUpScreen,
  UserInfo ,
  ChatWindow  ,
  GroupChatWindow,
  SearchChatList,
  GroupsChatList
});

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));